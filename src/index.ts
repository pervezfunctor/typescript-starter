import { map, pipe } from 'utils'
import './style.css'

console.log(
  pipe(
    [1, 2, 3, 4],
    map(x => x + 1),
  ),
)

if (module.hot !== undefined) {
  module.hot.accept()
}
